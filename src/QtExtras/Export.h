#ifndef MAKE_DLL
#    error "MAKE_DLL must be defined as MAKE_DLL_FooModule before including Export.h"
#endif
#ifdef DLL_STATE
#    undef DLL_STATE
#endif
#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#    if MAKE_DLL == 1
#        define DLL_STATE __declspec(dllexport)
#    else
#        define DLL_STATE __declspec(dllimport)
#    endif
#else
#    define DLL_STATE
#endif
#undef MAKE_DLL
