//Taken form SpanSlider

/****************************************************************************
 **
 ** Copyright (C) Qxt Foundation. Some rights reserved.
 **
 ** This file is part of the QxtGui module of the Qxt library.
 **
 ** This library is free software; you can redistribute it and/or modify it
 ** under the terms of the Common Public License, version 1.0, as published
 ** by IBM, and/or under the terms of the GNU Lesser General Public License,
 ** version 2.1, as published by the Free Software Foundation.
 **
 ** This file is provided "AS IS", without WARRANTIES OR CONDITIONS OF ANY
 ** KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY
 ** WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR
 ** FITNESS FOR A PARTICULAR PURPOSE.
 **
 ** You should have received a copy of the CPL and the LGPL along with this
 ** file. See the LICENSE file and the cpl1.0.txt/lgpl-2.1.txt files
 ** If you did not receive a copy of the licenses, contact the Qxt Foundation.
 **
 ** <http://libqxt.org>  <foundation@libqxt.org>
 **
 ****************************************************************************/

#include "SpanSlider.h"
#include <QKeyEvent>
#include <QStylePainter>
#include <QStyleOptionSlider>

    class SpanSliderPrivate
    {
    public:

	SpanSliderPrivate(SpanSlider *parent);
	~SpanSliderPrivate();
        void initStyleOption(QStyleOptionSlider* option, SpanSlider::SpanHandle handle = SpanSlider::UpperHandle) const;
	int pick(const QPoint& pt) const
	{
	    return parent_->orientation() == Qt::Horizontal ? pt.x() : pt.y();
	}
	int pixelPosToRangeValue(int pos) const;
	void handleMousePress(const QPoint& pos, QStyle::SubControl& control, int value, SpanSlider::SpanHandle handle);
	void drawHandle(QStylePainter* painter, SpanSlider::SpanHandle handle) const;
	void setupPainter(QPainter* painter, Qt::Orientation orientation, qreal x1, qreal y1, qreal x2, qreal y2) const;
	void drawSpan(QStylePainter* painter, const QRect& rect) const;
	void triggerAction(QAbstractSlider::SliderAction action, bool main);
	void swapControls();

	/**Parent public class*/
	SpanSlider *parent_;
		
	int lower;
	int upper;
	int lowerPos;
	int upperPos;
	int offset;
	int position;
	SpanSlider::SpanHandle lastPressed;
	SpanSlider::SpanHandle mainControl;
	QStyle::SubControl lowerPressed;
	QStyle::SubControl upperPressed;
	SpanSlider::HandleMovementMode movement;
	bool firstMovement;
	bool blockTracking;
	
    };

    SpanSliderPrivate::SpanSliderPrivate(SpanSlider *parent) :
	    parent_(parent),
	    lower(0),
	    upper(0),
	    lowerPos(0),
	    upperPos(0),
	    offset(0),
	    position(0),
	    lastPressed(SpanSlider::NoHandle),
	    mainControl(SpanSlider::LowerHandle),
	    lowerPressed(QStyle::SC_None),
	    upperPressed(QStyle::SC_None),
	    movement(SpanSlider::FreeMovement),
	    firstMovement(false),
	    blockTracking(false)
    {
    }
    
    SpanSliderPrivate::~SpanSliderPrivate() 
    {
    }

    // TODO: get rid of this in Qt 4.3
    void SpanSliderPrivate::initStyleOption(QStyleOptionSlider* option, SpanSlider::SpanHandle handle) const
    {
	if (!option)
	    return;

	const QSlider* p = parent_;
	option->initFrom(p);
	option->subControls = QStyle::SC_None;
	option->activeSubControls = QStyle::SC_None;
	option->orientation = p->orientation();
	option->maximum = p->maximum();
	option->minimum = p->minimum();
	option->tickPosition = p->tickPosition();
	option->tickInterval = p->tickInterval();
	option->upsideDown = (p->orientation() == Qt::Horizontal) ?
			    (p->invertedAppearance() != (option->direction == Qt::RightToLeft)) : (!p->invertedAppearance());
	option->direction = Qt::LeftToRight; // we use the upsideDown option instead
	option->sliderPosition = (handle == SpanSlider::LowerHandle ? lowerPos : upperPos);
	option->sliderValue = (handle == SpanSlider::LowerHandle ? lower : upper);
	option->singleStep = p->singleStep();
	option->pageStep = p->pageStep();
	if (p->orientation() == Qt::Horizontal)
	    option->state |= QStyle::State_Horizontal;
    }

    int SpanSliderPrivate::pixelPosToRangeValue(int pos) const
    {
	QStyleOptionSlider opt;
	initStyleOption(&opt);

	int sliderMin = 0;
	int sliderMax = 0;
	int sliderLength = 0;
	const QSlider* p = parent_;
	const QRect gr = p->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderGroove, p);
	const QRect sr = p->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, p);
	if (p->orientation() == Qt::Horizontal)
	{
	    sliderLength = sr.width();
	    sliderMin = gr.x();
	    sliderMax = gr.right() - sliderLength + 1;
	}
	else
	{
	    sliderLength = sr.height();
	    sliderMin = gr.y();
	    sliderMax = gr.bottom() - sliderLength + 1;
	}
	return QStyle::sliderValueFromPosition(p->minimum(), p->maximum(), pos - sliderMin,
					      sliderMax - sliderMin, opt.upsideDown);
    }

    void SpanSliderPrivate::handleMousePress(const QPoint& pos, QStyle::SubControl& control, int value, SpanSlider::SpanHandle handle)
    {
	QStyleOptionSlider opt;
	initStyleOption(&opt, handle);
	SpanSlider* p = parent_;
	const QStyle::SubControl oldControl = control;
	control = p->style()->hitTestComplexControl(QStyle::CC_Slider, &opt, pos, p);
	const QRect sr = p->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, p);
	if (control == QStyle::SC_SliderHandle)
	{
	    position = value;
	    offset = pick(pos - sr.topLeft());
	    lastPressed = handle;
	    p->setSliderDown(true);
	    p->sliderPressed(handle); //YVH
	}
	if (control != oldControl)
	    p->update(sr);
    }

    void SpanSliderPrivate::setupPainter(QPainter* painter, Qt::Orientation orientation, qreal x1, qreal y1, qreal x2, qreal y2) const
    {
	QColor highlight = parent_->palette().color(QPalette::Highlight);
	QLinearGradient gradient(x1, y1, x2, y2);
	gradient.setColorAt(0, highlight.dark(120));
	gradient.setColorAt(1, highlight.light(108));
	painter->setBrush(gradient);

	if (orientation == Qt::Horizontal)
	    painter->setPen(QPen(highlight.dark(130), 0));
	else
	    painter->setPen(QPen(highlight.dark(150), 0));
    }

    void SpanSliderPrivate::drawSpan(QStylePainter* painter, const QRect& rect) const
    {
	QStyleOptionSlider opt;
	initStyleOption(&opt);
	const QSlider* p = parent_;

	// area
	QRect groove = p->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderGroove, p);
	if (opt.orientation == Qt::Horizontal)
	    groove.adjust(0, 0, -1, 0);
	else
	    groove.adjust(0, 0, 0, -1);

	// pen & brush
	painter->setPen(QPen(p->palette().color(QPalette::Dark).light(110), 0));
	if (opt.orientation == Qt::Horizontal)
	    setupPainter(painter, opt.orientation, groove.center().x(), groove.top(), groove.center().x(), groove.bottom());
	else
	    setupPainter(painter, opt.orientation, groove.left(), groove.center().y(), groove.right(), groove.center().y());

	// draw groove
	painter->drawRect(rect.intersected(groove));
    }

    void SpanSliderPrivate::drawHandle(QStylePainter* painter, SpanSlider::SpanHandle handle) const
    {
	QStyleOptionSlider opt;
	initStyleOption(&opt, handle);
	opt.subControls = QStyle::SC_SliderHandle;
	QStyle::SubControl pressed = (handle == SpanSlider::LowerHandle ? lowerPressed : upperPressed);
	if (pressed == QStyle::SC_SliderHandle)
	{
	    opt.activeSubControls = pressed;
	    opt.state |= QStyle::State_Sunken;
	}
	painter->drawComplexControl(QStyle::CC_Slider, opt);
    }

    void SpanSliderPrivate::triggerAction(QAbstractSlider::SliderAction action, bool main)
    {
	int value = 0;
	bool no = false;
	bool up = false;
	const int min = parent_->minimum();
	const int max = parent_->maximum();
	const SpanSlider::SpanHandle altControl = (mainControl == SpanSlider::LowerHandle ? SpanSlider::UpperHandle : SpanSlider::LowerHandle);

	blockTracking = true;

	switch (action)
	{
	case QAbstractSlider::SliderSingleStepAdd:
	    if ((main && mainControl == SpanSlider::UpperHandle) || (!main && altControl == SpanSlider::UpperHandle))
	    {
		value = qBound(min, upper + parent_->singleStep(), max);
		up = true;
		break;
	    }
	    value = qBound(min, lower + parent_->singleStep(), max);
	    break;
	case QAbstractSlider::SliderSingleStepSub:
	    if ((main && mainControl == SpanSlider::UpperHandle) || (!main && altControl == SpanSlider::UpperHandle))
	    {
		value = qBound(min, upper - parent_->singleStep(), max);
		up = true;
		break;
	    }
	    value = qBound(min, lower - parent_->singleStep(), max);
	    break;
	case QAbstractSlider::SliderToMinimum:
	    value = min;
	    if ((main && mainControl == SpanSlider::UpperHandle) || (!main && altControl == SpanSlider::UpperHandle))
		up = true;
	    break;
	case QAbstractSlider::SliderToMaximum:
	    value = max;
	    if ((main && mainControl == SpanSlider::UpperHandle) || (!main && altControl == SpanSlider::UpperHandle))
		up = true;
	    break;
	case QAbstractSlider::SliderMove:
	  if ((main && mainControl == SpanSlider::UpperHandle) || (!main && altControl == SpanSlider::UpperHandle))
	    up = true;	  
	  no = true; // There was no break statement hereinitially, but this generates a warning with newer compilers.
	  // We add no=true here, but what is the point of setting up=true earlier? It won't be used anyway. But all in all
	  // it seems to work anyway.
	  break;
	case QAbstractSlider::SliderNoAction:
	  no = true;
	  break;
	default:
	  qWarning("SpanSliderPrivate::triggerAction: Unknown action");
	  break;
	}

	if (!no && !up)
	{
	    if (movement == SpanSlider::NoCrossing)
		value = qMin(value, upper);
	    else if (movement == SpanSlider::NoOverlapping)
		value = qMin(value, upper - 1);

	    if (movement == SpanSlider::FreeMovement && value > upper)
	    {
		swapControls();
		parent_->setUpperPosition(value);
	    }
	    else
	    {
		parent_->setLowerPosition(value);
	    }
	}
	else if (!no)
	{
	    if (movement == SpanSlider::NoCrossing)
		value = qMax(value, lower);
	    else if (movement == SpanSlider::NoOverlapping)
		value = qMax(value, lower + 1);

	    if (movement == SpanSlider::FreeMovement && value < lower)
	    {
		swapControls();
		parent_->setLowerPosition(value);
	    }
	    else
	    {
		parent_->setUpperPosition(value);
	    }
	}

	blockTracking = false;
	parent_->setLowerValue(lowerPos);
	parent_->setUpperValue(upperPos);
    }

    void SpanSliderPrivate::swapControls()
    {
	qSwap(lower, upper);
	qSwap(lowerPressed, upperPressed);
	lastPressed = (lastPressed == SpanSlider::LowerHandle ? SpanSlider::UpperHandle : SpanSlider::LowerHandle);
	mainControl = (mainControl == SpanSlider::LowerHandle ? SpanSlider::UpperHandle : SpanSlider::LowerHandle);
    }

    /*!
	\class SpanSlider
	\inmodule QxtGui
	\brief The SpanSlider widget is a QSlider with two handles.

	SpanSlider is a slider with two handles. SpanSlider is
	handy for letting user to choose an span between min/max.

	The span color is calculated based on QPalette::Highlight.

	The keys are bound according to the following table:
	\table
	\header \o Orientation    \o Key           \o Handle
	\row    \o Qt::Horizontal \o Qt::Key_Left  \o lower
	\row    \o Qt::Horizontal \o Qt::Key_Right \o lower
	\row    \o Qt::Horizontal \o Qt::Key_Up    \o upper
	\row    \o Qt::Horizontal \o Qt::Key_Down  \o upper
	\row    \o Qt::Vertical   \o Qt::Key_Up    \o lower
	\row    \o Qt::Vertical   \o Qt::Key_Down  \o lower
	\row    \o Qt::Vertical   \o Qt::Key_Left  \o upper
	\row    \o Qt::Vertical   \o Qt::Key_Right \o upper
	\endtable

	Keys are bound by the time the slider is created. A key is bound
	to same handle for the lifetime of the slider. So even if the handle
	representation might change from lower to upper, the same key binding
	remains.

	\image qxtspanslider.png "SpanSlider in Plastique style."

	\bold {Note:} SpanSlider inherits QSlider for implementation specific
	reasons. Adjusting any single handle specific properties like
	\list
	\o QAbstractSlider::sliderPosition
	\o QAbstractSlider::value
	\endlist
	has no effect. However, all slider specific properties like
	\list
	\o QAbstractSlider::invertedAppearance
	\o QAbstractSlider::invertedControls
	\o QAbstractSlider::minimum
	\o QAbstractSlider::maximum
	\o QAbstractSlider::orientation
	\o QAbstractSlider::pageStep
	\o QAbstractSlider::singleStep
	\o QSlider::tickInterval
	\o QSlider::tickPosition
	\endlist
	are taken into consideration.
    */

    /*!
	\enum SpanSlider::HandleMovementMode

	This enum describes the available handle movement modes.

	\value FreeMovement The handles can be moved freely.
	\value NoCrossing The handles cannot cross, but they can still overlap each other. The lower and upper values can be the same.
	\value NoOverlapping The handles cannot overlap each other. The lower and upper values cannot be the same.
    */

    /*!
	\fn SpanSlider::lowerValueChanged(int lower)

	This signal is emitted whenever the \a lower value has changed.
    */

    /*!
	\fn SpanSlider::upperValueChanged(int upper)

	This signal is emitted whenever the \a upper value has changed.
    */

    /*!
	\fn SpanSlider::spanChanged(int lower, int upper)

	This signal is emitted whenever both the \a lower and the \a upper
	values have changed ie. the span has changed.
    */

    /*!
	\fn SpanSlider::lowerPositionChanged(int lower)

	This signal is emitted whenever the \a lower position has changed.
    */

    /*!
	\fn SpanSlider::upperPositionChanged(int upper)

	This signal is emitted whenever the \a upper position has changed.
    */

    /*!
	Constructs a new SpanSlider with \a parent.
    */
    SpanSlider::SpanSlider(QWidget* parent) : QSlider(parent)
    {
	setOrientation(Qt::Horizontal);
	p_ = new SpanSliderPrivate(this);
	connect(this, SIGNAL(rangeChanged(int, int)), this, SLOT(setSpan(int, int)));
	connect(this, SIGNAL(sliderReleased()), this, SLOT(movePressedHandle()));
    }

    /*!
	Constructs a new SpanSlider with \a orientation and \a parent.
    */
    SpanSlider::SpanSlider(Qt::Orientation orientation, QWidget* parent) : QSlider(orientation, parent)
    {
	setOrientation(Qt::Horizontal);
	p_ = new SpanSliderPrivate(this);
	connect(this, SIGNAL(rangeChanged(int, int)), this, SLOT(setSpan(int, int)));
	connect(this, SIGNAL(sliderReleased()), this, SLOT(movePressedHandle()));
    }

    /*!
	Destructs the span slider.
    */
    SpanSlider::~SpanSlider()
    {
      if (p_) delete p_;
    }

    /*!
	\property SpanSlider::handleMovementMode
	\brief the handle movement mode
    */
    SpanSlider::HandleMovementMode SpanSlider::handleMovementMode() const
    {
	return p_->movement;
    }

    void SpanSlider::setHandleMovementMode(SpanSlider::HandleMovementMode mode)
    {
	p_->movement = mode;
    }

    /*!
	\property SpanSlider::lowerValue
	\brief the lower value of the span
    */
    int SpanSlider::lowerValue() const
    {
	return qMin(p_->lower, p_->upper);
    }

    void SpanSlider::setLowerValue(int lower)
    {
        //setSpan(lower, p_->upper);
        int upper = p_->upper;      
	const int low = qBound(minimum(), qMin(lower, upper), maximum());
	if (low != p_->lower)
	{
	    if (low != p_->lower)
	    {
		p_->lower = low;
		p_->lowerPos = low;
		emit lowerValueChanged(low);
	    }
	    emit spanChanged(p_->lower, p_->upper);
	    update();
	}
    }

    /*!
	\property SpanSlider::upperValue
	\brief the upper value of the span
    */
    int SpanSlider::upperValue() const
    {
	return qMax(p_->lower, p_->upper);
    }

    void SpanSlider::setUpperValue(int upper)
    {
        //setSpan(p_->lower, upper);
        int lower = p_->lower;
	const int upp = qBound(minimum(), qMax(lower, upper), maximum());
	if (upp != p_->upper)
	{
	    if (upp != p_->upper)
	    {
		p_->upper = upp;
		p_->upperPos = upp;
		emit upperValueChanged(upp);
	    }
	    emit spanChanged(p_->lower, p_->upper);
	    update();
	}
    }

    /*!
	Sets the span from \a lower to \a upper.
    */
    void SpanSlider::setSpan(int lower, int upper)
    {
	const int low = qBound(minimum(), qMin(lower, upper), maximum());
	const int upp = qBound(minimum(), qMax(lower, upper), maximum());
	if (low != p_->lower || upp != p_->upper)
	{
	    if (low != p_->lower)
	    {
		p_->lower = low;
		p_->lowerPos = low;
		emit lowerValueChanged(low);
	    }
	    if (upp != p_->upper)
	    {
		p_->upper = upp;
		p_->upperPos = upp;
		emit upperValueChanged(upp);
	    }
	    emit spanChanged(p_->lower, p_->upper);
	    update();
	}
    }

    /*!
	\property SpanSlider::lowerPosition
	\brief the lower position of the span
    */
    int SpanSlider::lowerPosition() const
    {
	return p_->lowerPos;
    }

    void SpanSlider::setLowerPosition(int lower)
    {
	if (p_->lowerPos != lower)
	{
	    p_->lowerPos = lower;
	    if (!hasTracking())
		update();
	    if (isSliderDown())
		emit lowerPositionChanged(lower);
	    if (hasTracking() && !p_->blockTracking)
	    {
	        bool main = (p_->mainControl == SpanSlider::LowerHandle);
		p_->triggerAction(SliderMove, main);
	    }
	}
    }

    /*!
	\property SpanSlider::upperPosition
	\brief the upper position of the span
    */
    int SpanSlider::upperPosition() const
    {
	return p_->upperPos;
    }

    void SpanSlider::setUpperPosition(int upper)
    {
	if (p_->upperPos != upper)
	{
	    p_->upperPos = upper;
	    if (!hasTracking())
		update();
	    if (isSliderDown())
		emit upperPositionChanged(upper);
	    if (hasTracking() && !p_->blockTracking)
	    {
		bool main = (p_->mainControl == SpanSlider::UpperHandle);
		p_->triggerAction(SliderMove, main);
	    }
	}
    }

    /*!
	\reimp
    */
    void SpanSlider::keyPressEvent(QKeyEvent* event)
    {
	QSlider::keyPressEvent(event);

	bool main = true;
	SliderAction action = SliderNoAction;
	switch (event->key())
	{
	case Qt::Key_Left:
	    main   = (orientation() == Qt::Horizontal);
	    action = !invertedAppearance() ? SliderSingleStepSub : SliderSingleStepAdd;
	    break;
	case Qt::Key_Right:
	    main   = (orientation() == Qt::Horizontal);
	    action = !invertedAppearance() ? SliderSingleStepAdd : SliderSingleStepSub;
	    break;
	case Qt::Key_Up:
	    main   = (orientation() == Qt::Vertical);
	    action = invertedControls() ? SliderSingleStepSub : SliderSingleStepAdd;
	    break;
	case Qt::Key_Down:
	    main   = (orientation() == Qt::Vertical);
	    action = invertedControls() ? SliderSingleStepAdd : SliderSingleStepSub;
	    break;
	case Qt::Key_Home:
	    main   = (p_->mainControl == SpanSlider::LowerHandle);
	    action = SliderToMinimum;
	    break;
	case Qt::Key_End:
	    main   = (p_->mainControl == SpanSlider::UpperHandle);
	    action = SliderToMaximum;
	    break;
	default:
	    event->ignore();
	    break;
	}

	if (action)
	    p_->triggerAction(action, main);
    }

    /*!
	\reimp
    */
    void SpanSlider::mousePressEvent(QMouseEvent* event)
    {
	if (minimum() == maximum() || (event->buttons() ^ event->button()))
	{
	    event->ignore();
	    return;
	}

	p_->handleMousePress(event->pos(), p_->upperPressed, p_->upper, SpanSlider::UpperHandle);
	if (p_->upperPressed != QStyle::SC_SliderHandle)
	    p_->handleMousePress(event->pos(), p_->lowerPressed, p_->lower, SpanSlider::LowerHandle);

	p_->firstMovement = true;
	event->accept();
    }

    /*!
	\reimp
    */
    void SpanSlider::mouseMoveEvent(QMouseEvent* event)
    {
	if (p_->lowerPressed != QStyle::SC_SliderHandle && p_->upperPressed != QStyle::SC_SliderHandle)
	{
	    event->ignore();
	    return;
	}

	QStyleOptionSlider opt;
	p_->initStyleOption(&opt);
	const int m = style()->pixelMetric(QStyle::PM_MaximumDragDistance, &opt, this);
	int newPosition = p_->pixelPosToRangeValue(p_->pick(event->pos()) - p_->offset);
	if (m >= 0)
	{
	    const QRect r = rect().adjusted(-m, -m, m, m);
	    if (!r.contains(event->pos()))
	    {
		newPosition = p_->position;
	    }
	}

	// pick the preferred handle on the first movement
	if (p_->firstMovement)
	{
	    if (p_->lower == p_->upper)
	    {
		if (newPosition < lowerValue())
		{
		    p_->swapControls();
		    p_->firstMovement = false;
		}
	    }
	    else
	    {
		p_->firstMovement = false;
	    }
	}

	if (p_->lowerPressed == QStyle::SC_SliderHandle)
	{
	    if (p_->movement == NoCrossing)
		newPosition = qMin(newPosition, upperValue());
	    else if (p_->movement == NoOverlapping)
		newPosition = qMin(newPosition, upperValue() - 1);

	    if (p_->movement == FreeMovement && newPosition > p_->upper)
	    {
		p_->swapControls();
		setUpperPosition(newPosition);
	    }
	    else
	    {
		setLowerPosition(newPosition);
	    }
	}
	else if (p_->upperPressed == QStyle::SC_SliderHandle)
	{
	    if (p_->movement == NoCrossing)
		newPosition = qMax(newPosition, lowerValue());
	    else if (p_->movement == NoOverlapping)
		newPosition = qMax(newPosition, lowerValue() + 1);

	    if (p_->movement == FreeMovement && newPosition < p_->lower)
	    {
		p_->swapControls();
		setLowerPosition(newPosition);
	    }
	    else
	    {
		setUpperPosition(newPosition);
	    }
	}
	event->accept();
    }

    /*!
	\reimp
    */
    void SpanSlider::mouseReleaseEvent(QMouseEvent* event)
    {
	QSlider::mouseReleaseEvent(event);
	setSliderDown(false);
	p_->lowerPressed = QStyle::SC_None;
	p_->upperPressed = QStyle::SC_None;
	update();
    }

    /*!
	\reimp
    */
    void SpanSlider::paintEvent(QPaintEvent* event)
    {
	Q_UNUSED(event);
	QStylePainter painter(this);

	// ticks
	QStyleOptionSlider opt;
	p_->initStyleOption(&opt);
	opt.sliderValue = 0;
	opt.sliderPosition = 0;
	opt.subControls = QStyle::SC_SliderGroove | QStyle::SC_SliderTickmarks;
	painter.drawComplexControl(QStyle::CC_Slider, opt);

	// handle rects
	opt.sliderPosition = p_->lowerPos;
	const QRect lr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
	const int lrv  = p_->pick(lr.center());
	opt.sliderPosition = p_->upperPos;
	const QRect ur = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
	const int urv  = p_->pick(ur.center());

	// span
	const int minv = qMin(lrv, urv);
	const int maxv = qMax(lrv, urv);
	//const QPoint c = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderGroove, this).center();
	const QPoint c = QRect(lr.center(), ur.center()).center();
	QRect spanRect;
	if (orientation() == Qt::Horizontal)
	    spanRect = QRect(QPoint(minv, c.y() - 2), QPoint(maxv, c.y() + 1));
	else
	    spanRect = QRect(QPoint(c.x() - 2, minv), QPoint(c.x() + 1, maxv));
	p_->drawSpan(&painter, spanRect);

	// handles
	switch (p_->lastPressed)
	{
	case SpanSlider::LowerHandle:
	    p_->drawHandle(&painter, SpanSlider::UpperHandle);
	    p_->drawHandle(&painter, SpanSlider::LowerHandle);
	    break;
	case SpanSlider::UpperHandle:
	default:
	    p_->drawHandle(&painter, SpanSlider::LowerHandle);
	    p_->drawHandle(&painter, SpanSlider::UpperHandle);
	    break;
	}
    }
    
    void SpanSlider::movePressedHandle()
    {
	switch (p_->lastPressed)
	{
	    case SpanSlider::LowerHandle:
		if (p_->lowerPos != p_->lower)
		{
		    bool main = (p_->mainControl == SpanSlider::LowerHandle);
		    p_->triggerAction(QAbstractSlider::SliderMove, main);
		}
		break;
	    case SpanSlider::UpperHandle:
		if (p_->upperPos != p_->upper)
		{
		    bool main = (p_->mainControl == SpanSlider::UpperHandle);
		    p_->triggerAction(QAbstractSlider::SliderMove, main);
		}
		break;
	    default:
		break;
	}
    }
